Project setup:
1. Create a database called 'coding_chiefs_exam' and create a user for the database with the following credentials
    username = "admin"
	password = "test_admin"
2. Import the contents of the database found in utils/coding_chiefs_exam.sql
3. Host a php server using the command: php -S localhost:3000
4. Open your browser window and go to localhost:3000

How to navigate
1. The main category and subcategory can be drilled down by clicking on the label
2. The subcategory and subproducts in each subcategory have checkboxes and the user can tick on any of them
    Note: "Select all" checkboxes are available for the subcategory and subproducts
3. After checking the desired subcategories and subproducts, click the "Add all selected" button to populate the content of the cart
4. Each item in a cart can be removed by clicking on the X button beside each one
5. The "Remove all selected" button clears the cart as well as selection in the tree