<style>
	label {
		display:block;
	}
</style>
<?php 
	require("./utils/conn.php");
	global $conn;
	$gender_stmt = "SELECT * FROM genders";
	$genders = mysqli_query($conn, $gender_stmt);
	while (mysqli_fetch_assoc($genders)){
		foreach ($genders as $gender) {
			echo "<label id='genderlabel'>{$gender['name']}</label>";

			$category_stmt = "SELECT * FROM categories WHERE parent_id = {$gender['id']}";
			$categories = mysqli_query($conn, $category_stmt);
			while(mysqli_fetch_assoc($categories)){
				foreach ($categories as $category) {
					echo "<label class='categorieslabels'>|-------{$category['name']}</label>";

					$subcat_stmt = "SELECT * FROM subcategories WHERE category_id = {$category['id']}";
					$subcats = mysqli_query($conn, $subcat_stmt);
					while(mysqli_fetch_assoc($subcats)){
						foreach($subcats as $subcat){
							echo "<label class='subcatlabels'>|-------------<input type='checkbox' id='subcat{$subcat['id']}' class='subcatchecks' data-parent={$subcat['category_id']} data-label={$subcat['id']} data-value={$subcat['name']}>{$subcat['name']}</label>";

							$subprod_stmt = "SELECT * FROM subproducts WHERE subcat_id = {$subcat['id']}";
							$subprods = mysqli_query($conn, $subprod_stmt);
							foreach($subprods as $subprod){
								echo "<label class='subprodlabels'>|---------------------<input type='checkbox' id='subprod{$subprod['id']}' class='subprodchecks' data-parent={$subprod['subcat_id']} data-label={$subprod['id']} data-value={$subprod['name']}>{$subprod['name']}</label>";
							}
							if (mysqli_num_rows($subprods)>0){
								echo "<label class='selectAllsubprod'>|---------------------<input type='checkbox' id='allsubcat{$subcat['id']}' data-parent='{$subcat['id']}' class='selectallsubprodchecks'>Select All</label>";
							}
						}

						echo "<label class='selectAllsubcat'>|-------------<input type='checkbox' id='allcat{$category['id']}' data-parent='{$category['id']}' class='selectallsubcatchecks'>Select All</label>";
					}
				}
			}
		}
	}
	mysqli_close($conn);
?>
<button id="addAll">Add all selected</button>
<button id="removeAll">Remove all selected</button>
<h3>Item cart</h3>
<ul id="cart">
</ul>
<script src="./js/util.js"></script>
