let genderchk = document.querySelector("#genderlabel");
let catlabel = document.querySelectorAll(".categorieslabels");
let subcatlabel = document.querySelectorAll(".subcatlabels");
let subcatchk = document.querySelectorAll(".subcatchecks");
let subprodlabel = document.querySelectorAll(".subprodlabels");
let subprodchk = document.querySelectorAll(".subprodchecks");
let selectAllsubprod = document.querySelectorAll(".selectAllsubprod");
let selectAllsubprodchk = document.querySelectorAll(".selectallsubprodchecks");
let selectAllsubcat = document.querySelectorAll(".selectAllsubcat");
let selectAllsubcatchk = document.querySelectorAll(".selectAllsubcatchecks");
let remAll = document.querySelector("#removeAll");
let addAll = document.querySelector("#addAll");
let remOne = document.querySelector(".removeIndiv");

//start of page
for (i = 0; i < catlabel.length; i++) {
	catlabel[i].style.display = "none";
}

for (i = 0; i < subcatlabel.length; i++) {
	subcatlabel[i].style.display = "none";
	subcatchk[i].checked = false;
}

for (i = 0; i < subprodlabel.length; i++) {
	subprodlabel[i].style.display = "none";
	subprodchk[i].checked = false;
}

for (i = 0; i < selectAllsubprod.length; i++) {
	selectAllsubprod[i].style.display = "none";
	selectAllsubprodchk[i].checked = false;
}

for (i = 0; i < selectAllsubcat.length; i++) {
	selectAllsubcat[i].style.display = "none";
	selectAllsubcatchk[i].checked = false;
}
//end of setup

genderchk.addEventListener("click", (e) => {
    e.stopPropagation
	if(catlabel[0].style.display == "none"){
		for (i = 0; i < catlabel.length; i++) {
			catlabel[i].style.display = "block";
		}
	} else {
		for (i = 0; i < catlabel.length; i++) {
			catlabel[i].style.display = "none";
		}
		for (i = 0; i < subcatlabel.length; i++) {
			subcatlabel[i].style.display = "none";
			subcatchk[i].checked = false;
		}
		for (i = 0; i < subprodlabel.length; i++) {
			subprodlabel[i].style.display = "none";
			subprodchk[i].checked = false;
		}
		for (i = 0; i < selectAllsubprod.length; i++) {
			selectAllsubprod[i].style.display = "none";
			selectAllsubprodchk[i].checked = false;
		}
		for (i = 0; i < selectAllsubcat.length; i++) {
			selectAllsubcat[i].style.display = "none";
			selectAllsubcatchk[i].checked = false;
		}
	}
})

for (i = 0; i < catlabel.length; i++) {
	catlabel[i].addEventListener("click", (e) => {
		e.stopPropagation;
		e.preventDefault;
			if(subcatlabel[0].style.display == "none"){
				for (i = 0; i < subcatlabel.length; i++) {
					subcatlabel[i].style.display = "block";
					subcatchk[i].checked = false;
				}
				for (i = 0; i < selectAllsubcat.length; i++) {
					selectAllsubcat[i].style.display = "block";
					selectAllsubcatchk[i].checked = false;
				}
			} else {
				for (i = 0; i < subcatlabel.length; i++) {
					subcatlabel[i].style.display = "none";
					subcatchk[i].checked = false;
				}
				for (i = 0; i < subprodlabel.length; i++) {
					subprodlabel[i].style.display = "none";
					subprodchk[i].checked = false;
				}
				for (i = 0; i < selectAllsubprod.length; i++) {
					selectAllsubprod[i].style.display = "none";
					selectAllsubprodchk[i].checked = false;
				}
				for (i = 0; i < selectAllsubcat.length; i++) {
					selectAllsubcat[i].style.display = "none";
					selectAllsubcatchk[i].checked = false;
				}
			}
	})
}

for (i = 0; i < subcatchk.length; i++) {
	subcatchk[i].addEventListener("change", (e) => {
		e.stopPropagation;
		e.preventDefault;
		if(e.target.checked){
			for (i = 0; i < subprodlabel.length; i++) {
				if (e.target.getAttribute("data-label") == subprodchk[i].getAttribute("data-parent")) {
					subprodlabel[i].style.display = "block";
					subprodchk[i].checked = false;
				}
			}
			for (i = 0; i < selectAllsubprod.length; i++) {
				if (e.target.getAttribute("data-label") == selectAllsubprodchk[i].getAttribute("data-parent")) {
					selectAllsubprod[i].style.display = "block";
					selectAllsubprodchk[i].checked = false;
				}
			}
		} else {
			for (i = 0; i < subprodlabel.length; i++) {
				if (e.target.getAttribute("data-label") == subprodchk[i].getAttribute("data-parent")) {
					subprodlabel[i].style.display = "none";
					subprodchk[i].checked = false;
				}
			}
			for (i = 0; i < selectAllsubprod.length; i++) {
				if (e.target.getAttribute("data-label") == selectAllsubprodchk[i].getAttribute("data-parent")) {
					selectAllsubprod[i].style.display = "none";
					selectAllsubprodchk[i].checked = false;
				}
			}
		}
	})
}

for (i = 0; i < selectAllsubprodchk.length; i++){
	selectAllsubprodchk[i].addEventListener("change", (e) => {
		e.stopPropagation;
		e.preventDefault;
		if (e.target.checked){
			for (i = 0; i < subprodlabel.length; i++) {
				if (e.target.getAttribute("data-parent") == subprodchk[i].getAttribute("data-parent")) {
					subprodchk[i].checked = true;
				}
			}
		} else {
			for (i = 0; i < subprodlabel.length; i++) {
				if (e.target.getAttribute("data-parent") == subprodchk[i].getAttribute("data-parent")) {
					subprodchk[i].checked = false;
				}
			}
			for (i = 0; i < selectAllsubcat.length; i++) {
				selectAllsubcatchk[i].checked = false;
			}
		}
	})
}

for (i = 0; i < selectAllsubcatchk.length; i++){
	selectAllsubcatchk[i].addEventListener("change", (e) => {
		e.stopPropagation;
		e.preventDefault;
		if (e.target.checked){
			for (i = 0; i < subcatlabel.length; i++) {
					subcatchk[i].checked = true;
			}
			for (i = 0; i < subprodlabel.length; i++) {
					subprodlabel[i].style.display = "block";
					subprodchk[i].checked = true;
			}
			for (i = 0; i < selectAllsubprod.length; i++) {
					selectAllsubprod[i].style.display = "block";
					selectAllsubprodchk[i].checked = true;
			}
		} else {
			for (i = 0; i < subcatlabel.length; i++) {
					subcatchk[i].checked = false;
			}
			for (i = 0; i < subprodlabel.length; i++) {
					subprodlabel[i].style.display = "none";
					subprodchk[i].checked = false;
			}
			for (i = 0; i < selectAllsubprod.length; i++) {
					selectAllsubprod[i].style.display = "none";
					selectAllsubprodchk[i].checked = false;
			}
		}
	})
}

remAll.addEventListener("click", () => {
	
	for (i = 0; i < subcatlabel.length; i++) {
		subcatchk[i].checked = false;
	}
	
	for (i = 0; i < subprodlabel.length; i++) {
		subprodchk[i].checked = false;
	}
	
	for (i = 0; i < selectAllsubprod.length; i++) {
		selectAllsubprodchk[i].checked = false;
	}
	
	for (i = 0; i < selectAllsubcat.length; i++) {
		selectAllsubcatchk[i].checked = false;
	}

    let cart = document.querySelector("#cart")
    cart.innerHTML = ""
})

addAll.addEventListener("click", () => {
    let cart = document.querySelector("#cart")
    cart.innerHTML = ""
    for (i = 0; i < subcatchk.length; i++) {
		if(subcatchk[i].checked){
            if(!(subcatchk[i].getAttribute('data-label') == 5 || subcatchk[i].getAttribute('data-label') == 7)){
                let cartItem = document.createElement("li");
                let content = subcatchk[i].getAttribute('data-value')
                cartItem.appendChild(document.createTextNode(content));
                let btn = document.createElement("button");
                btn.classList.add("removeIndiv");
                btn.innerHTML = "X";
                cartItem.appendChild(btn);
                btn.addEventListener("click", (e) => {
                    e.currentTarget.parentNode.remove();
                })
                cart.appendChild(cartItem);
            }
        }
	}

    for (i = 0; i < subprodchk.length; i++) {
		if(subprodchk[i].checked){
            let cartItem = document.createElement("li");
            let content = subprodchk[i].getAttribute('data-value')
            cartItem.appendChild(document.createTextNode(content));
            let btn = document.createElement("button");
            btn.classList.add("removeIndiv");
            btn.innerHTML = "X";
            cartItem.appendChild(btn);
            btn.addEventListener("click", (e) => {
                e.currentTarget.parentNode.remove();
            })
            cart.appendChild(cartItem);
        }
	}
});
